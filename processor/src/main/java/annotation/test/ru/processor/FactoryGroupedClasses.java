package annotation.test.ru.processor;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

class FactoryGroupedClasses {
    private static final String SUFFIX = "Factory";
    private String qualifiedClassName;
    private Map<String, FactoryAnnotatedClass> itemsMap = new LinkedHashMap<>();

    FactoryGroupedClasses(String qualifiedClassName) {
        this.qualifiedClassName = qualifiedClassName;
    }

    void add(FactoryAnnotatedClass item) throws ProcessingException {

        FactoryAnnotatedClass existing = itemsMap.get(item.getId());
        if (existing != null) {
            throw new ProcessingException(item.getTypeElement(),
                    "Conflict: The class %s is annotated with @%s with id ='%s' but %s already uses the same id",
                    item.getTypeElement().getQualifiedName().toString(),
                    Factory.class.getSimpleName(),
                    item.getId(),
                    existing.getTypeElement().getQualifiedName().toString());
        }

        itemsMap.put(item.getId(), item);
    }

    void generateCode(Elements elementUtils, Filer filer) throws IOException {
        final TypeElement superClassName = elementUtils.getTypeElement(qualifiedClassName);
        final String factoryClassName = superClassName.getSimpleName() + SUFFIX;
        final PackageElement pkg = elementUtils.getPackageOf(superClassName);
        final String packageName = pkg.isUnnamed() ? null : pkg.getQualifiedName().toString();

        final MethodSpec.Builder method = MethodSpec.methodBuilder("create")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(String.class, "id")
                .returns(TypeName.get(superClassName.asType()));

        method.beginControlFlow("if (id == null)")
                .addStatement("throw new IllegalArgumentException($S)", "id is null!")
                .endControlFlow();

        for (FactoryAnnotatedClass item : itemsMap.values()) {
            method.beginControlFlow("if ($S.equals(id))", item.getId())
                    .addStatement("return new $L()", item.getTypeElement().getQualifiedName().toString())
                    .endControlFlow();
        }

        method.addStatement("throw new IllegalArgumentException($S + id)", "Unknown id = ");

        final TypeSpec typeSpec = TypeSpec.classBuilder(factoryClassName).addMethod(method.build()).build();

        JavaFile.builder(packageName, typeSpec).build().writeTo(filer);
    }
}
