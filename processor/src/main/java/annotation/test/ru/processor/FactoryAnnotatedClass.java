package annotation.test.ru.processor;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;

public class FactoryAnnotatedClass {
    private TypeElement annotatedClassElement;
    private String qualifiedGroupClassName;
    private String simpleFactoryClassName;
    private String id;

    FactoryAnnotatedClass(TypeElement classElement) throws ProcessingException {
        annotatedClassElement = classElement;
        final Factory annotation = classElement.getAnnotation(Factory.class);
        id = annotation.id();

        if (id.isEmpty()) throw new ProcessingException(classElement,
                "id() in @%s for class %s is null or empty! that's not allowed",
                Factory.class.getSimpleName(), classElement.getQualifiedName().toString());

        try {
            Class<?> clazz = annotation.type();
            qualifiedGroupClassName = clazz.getCanonicalName();
            simpleFactoryClassName = clazz.getSimpleName();
        } catch (MirroredTypeException mte) {
            final DeclaredType classTypeMirror = (DeclaredType) mte.getTypeMirror();
            final TypeElement classTypeElement = (TypeElement) classTypeMirror.asElement();
            qualifiedGroupClassName = classTypeElement.getQualifiedName().toString();
            simpleFactoryClassName = classTypeElement.getSimpleName().toString();
        }
    }

    TypeElement getTypeElement() {
        return annotatedClassElement;
    }

    String getQualifiedFactoryGroupName() {
        return qualifiedGroupClassName;
    }

    public String getSimpleFactoryClassName() {
        return simpleFactoryClassName;
    }

    public String getId() {
        return id;
    }
}
