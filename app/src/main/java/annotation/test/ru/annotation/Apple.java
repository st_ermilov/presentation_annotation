package annotation.test.ru.annotation;

import annotation.test.ru.processor.Factory;

@Factory(id = "Apple", type = Fruit.class)
public class Apple extends Fruit {
}
