package annotation.test.ru.annotation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final FruitFactory factory = new FruitFactory();
        factory.create("Apple");
        factory.create("Plum");
    }
}
