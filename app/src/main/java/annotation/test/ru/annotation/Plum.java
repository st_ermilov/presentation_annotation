package annotation.test.ru.annotation;

import annotation.test.ru.processor.Factory;

@Factory(id = "Plum", type = Fruit.class)
public class Plum extends Fruit {
}
